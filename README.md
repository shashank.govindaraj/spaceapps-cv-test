## Programming test - Computer vision
Create the code to detect each can in the "/data/models_and_background" folder against the test images present in the "/data/test_images" folder.

Ex. Detect can1.jpg in test1.jpg, test2.jpg, test3.jpg...followed by detection of can2.jpg in test1.jpg, test2.jpg...

You may use either the c++ or the python structure. Make sure you have installed all necessary development packages to compile and execute c++ or python code, as well as opencv.


#### Steps to perform the test
1. Fork the repository to a private repository
2. Decide between C++ or python to perform the test. Any other language is also ok, but the available files will have to be re-written.
3. Install the necessary packages [instructions for Ubuntu 18.04] on the development computer. Any other version of Ubuntu is also ok. 
[**Bonus points & Optional**] This step could be done in a Docker container:
  1. C++: sudo apt install build-essential cmake pkg-config libopencv-dev
  2. Python:
    - sudo apt install python libopencv-dev python-opencv
    - sudo pip3 install opencv-python
  3. [**Bonus points & Optional**]Install ROS 1 or ROS2: http://wiki.ros.org/melodic/Installation/Ubuntu. Choose any ROS1/2 version that works for your Operating System.
4. Fill in the detect function with your code
5. [**Bonus points & Optional**] Write a ROS wrapper to send the detected bbox via a ROS message
6. Share the link to your forked repo with us, including the results.txt file
7. Provide a readme as much information as possible in the available time on the logic, build, run etc.
